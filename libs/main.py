# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of MyNotes project.
# Name:         MyNotes: a little notes soft
# Copyright:    (C) 2015-2019 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    contient l'interface graphique.
    La plupart des actions renvoient à des fonctions qui sont dans les
    modules utils_aaa.
"""

# importation des modules utiles :
import json

# importation des modules perso :
import utils
import utils_functions
import utils_webengine
import utils_filesdirs
import utils_editor
import utils_dragdrop

from PyQt5 import QtCore, QtWidgets, QtGui



class MainWindow(QtWidgets.QMainWindow):
    """
    LA FENÊTRE PRINCIPALE
    """
    def __init__(self, parent=None):
        """
        mise en place de l'interface
        plus des variables utiles
        """
        super(MainWindow, self).__init__(parent)

        utils.loadSupportedImageFormats()
        # le dossier du logiciel et des fichiers :
        self.beginDir = QtCore.QDir.currentPath()
        self.configDir, first = utils_filesdirs.createConfigAppDir(
            utils.PROGNAME)
        self.tempPath = utils_filesdirs.createTempAppDir(
            utils.PROGNAME + '-temp')

        self.notesDir = QtCore.QDir(
            '{0}/notes'.format(self.configDir.path()))
        if not(self.notesDir.exists()):
            self.configDir.mkdir('notes')
            self.configDir.mkdir('html')
            utils_filesdirs.copyDir(
                '{0}/files/md'.format(self.beginDir), 
                '{0}/html'.format(self.configDir.path()))

        # on lit le fichier de config :
        configDict = {'ORDER': []}
        configFileName = '{0}/config.json'.format(
            self.configDir.canonicalPath())
        try:
            if QtCore.QFile(configFileName).exists():
                configFile = open(
                    configFileName, newline='', encoding='utf-8')
                configDict = json.load(configFile)
                configFile.close()
        except:
            configDict = {'ORDER': []}

        # récupération de la liste des fichiers :
        notes = {'ORDER': []}
        temp = []
        dirIterator = QtCore.QDirIterator(self.notesDir)
        while dirIterator.hasNext():
            note = dirIterator.next()
            if QtCore.QFileInfo(note).suffix() in ('txt', 'md'):
                baseName = QtCore.QFileInfo(note).baseName()
                notes[baseName] = note
                temp.append(baseName)
        temp.sort()
        for note in configDict['ORDER']:
            if note in temp:
                notes['ORDER'].append(note)
        for note in temp:
            if not(note in configDict['ORDER']):
                notes['ORDER'].append(note)

        # le fichier actuel :
        self.actual = {'note': '', 'fileName': '', 'mustSave': False}
        # pour esquiver le premier appel à textChanged :
        self.noChange = True
        # pour esquiver noteChanged :
        self.noNoteChanged = False

        # mise en place de l'interface :
        self.createInterface(notes=notes)
        self.updateTitle()
        # redimensionnement de la fenêtre :
        rect = QtWidgets.QApplication.desktop().availableGeometry()
        w, h = int(rect.width() * 0.67), int(rect.height() * 0.67)
        self.resize(w, h)
        self.notesListWidget.itemSelectionChanged.connect(self.noteChanged)
        self.noteTextEdit.textChanged.connect(self.textChanged)

    def interfaceLoaded(self):
        """
        fonction appelée une fois que l'interface est affichée.
        Permet de régler les positionnements des éléments
        """
        #print('interfaceLoaded')
        newWidth = QtWidgets.QApplication.desktop(
            ).availableGeometry().width()
        self.previewFrame.setMaximumWidth(newWidth)
        self.hsplitter.moveSplitter(newWidth, 2)

    def createInterface(self, notes={'ORDER': []}):
        # barre d'outils, actions et menu :
        self.toolBar = QtWidgets.QToolBar(self)
        #self.toolBar.setOrientation(QtCore.Qt.Vertical)
        self.toolBar.setIconSize(QtCore.QSize(32, 32))
        self.toolBar.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)

        # la liste des fichiers :
        self.notesListWidget = utils_dragdrop.DragDropListWidget(extendedSelection=False)
        for note in notes['ORDER']:
            noteFile = notes[note]
            noteWidget = QtWidgets.QListWidgetItem(note)
            noteWidget.setData(QtCore.Qt.UserRole, noteFile)
            self.notesListWidget.addItem(noteWidget)
        self.notesListWidget.setMaximumWidth(200)

        # zone de recherche :
        self.lastMatch = None
        self.findField = QtWidgets.QLineEdit(self)
        findButton = QtWidgets.QPushButton(
            utils.doIcon('edit-find'), '')
        findButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Search'))
        #findButton.setFlat(True)
        searchLayout = QtWidgets.QHBoxLayout()
        searchLayout.addWidget(self.findField)
        searchLayout.addWidget(findButton)
        self.findField.editingFinished.connect(self.find)
        findButton.clicked.connect(self.find)
        # correcteur d'orthographe :
        spellCheckButton = QtWidgets.QPushButton(
            utils.doIcon('tools-check-spelling'), '')
        spellCheckButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Spell check'))
        spellCheckButton.setCheckable(True)
        #spellCheckButton.setFlat(True)
        spellCheckButton.clicked.connect(self.spellCheck)
        searchLayout.addWidget(spellCheckButton)
        # affichage du volet preview :
        self.previewPanelButton = QtWidgets.QPushButton(
            utils.doIcon('view-right-new'), '')
        self.previewPanelButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Show/Hide Preview Panel'))
        self.previewPanelButton.setCheckable(True)
        #self.previewPanelButton.setFlat(True)
        self.previewPanelButton.clicked.connect(self.showPreviewPanel)
        searchLayout.addWidget(self.previewPanelButton)
        # un QTextEdit pour afficher la note :
        self.noteTextEdit = utils_editor.markdownEditor(self)
        self.noteTextEdit.setDisabled(True)
        # mise en place :
        mdLayout = QtWidgets.QVBoxLayout()
        mdLayout.addLayout(searchLayout)
        mdLayout.addWidget(self.noteTextEdit)
        mdFrame = QtWidgets.QWidget()
        mdFrame.setLayout(mdLayout)

        # preview :
        previewButton = QtWidgets.QPushButton(
            utils.doIcon('document-preview'), '')
        previewButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Preview'))
        #previewButton.setFlat(True)
        previewButton.clicked.connect(self.preview)
        openButton = QtWidgets.QPushButton(
            utils.doIcon('applications-internet'), '')
        openButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Open in browser'))
        #openButton.setFlat(True)
        openButton.clicked.connect(self.openPreview)
        toolsLayout = QtWidgets.QHBoxLayout()
        toolsLayout.addStretch()
        toolsLayout.addWidget(previewButton)
        toolsLayout.addWidget(openButton)
        # un MyWebEngineView pour afficher :
        self.previewWebView = utils_webengine.MyWebEngineView(self)
        container = QtWidgets.QAbstractScrollArea()
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.previewWebView)
        container.setLayout(vBoxLayout)
        # mise en place :
        previewLayout = QtWidgets.QVBoxLayout()
        previewLayout.addLayout(toolsLayout)
        previewLayout.addWidget(container)
        self.previewFrame = QtWidgets.QWidget()
        self.previewFrame.setLayout(previewLayout)
        self.previewFrame.setMaximumWidth(0)

        # un splitter horizontal pour agencer les 3 :
        self.hsplitter = QtWidgets.QSplitter(self)
        self.hsplitter.setOrientation(QtCore.Qt.Horizontal)
        self.hsplitter.addWidget(self.notesListWidget)
        self.hsplitter.addWidget(mdFrame)
        self.hsplitter.addWidget(self.previewFrame)
        self.hsplitter.splitterMoved.connect(self.splitterMoved)

        # le stackedWidget est le widget central :
        self.stackedWidget = QtWidgets.QStackedWidget()
        self.stackedWidget.addWidget(self.hsplitter)
        self.setCentralWidget(self.stackedWidget)
        self.closing = False

        # le reste de l'interface :
        self.createActions()
        self.createMenusAndButtons()

    def createActions(self):
        self.actionNew = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'New'), 
            self, 
            icon=utils.doIcon('document-new'), 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.New))
        self.actionNew.triggered.connect(self.newFile)

        self.actionSave = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Save'), 
            self, 
            icon=utils.doIcon('document-save'), 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Save))
        self.actionSave.triggered.connect(self.saveFile)

        self.actionDoHtml = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Transform to html'), 
            self, 
            icon=utils.doIcon('html'))
        self.actionDoHtml.triggered.connect(self.doHtml)

        self.actionOpenNotesDir = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Open Notes Dir'), 
            self, 
            icon=utils.doIcon('folder-documents'))
        self.actionOpenNotesDir.triggered.connect(self.openNotesDir)

        self.actionOpenInTextEditor = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Open in text editor'), 
            self, 
            icon=utils.doIcon('accessories-text-editor'))
        self.actionOpenInTextEditor.triggered.connect(self.openInTextEditor)

        self.actionCreateDesktopFileLinux = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a launcher'), 
            self, 
            icon=utils.doIcon('logo_linux'))
        self.actionCreateDesktopFileLinux.triggered.connect(
            self.doCreateDesktopFileLinux)

        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Exit'), 
            self, 
            icon=utils.doIcon('application-exit'), 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Quit))
        self.actionQuit.triggered.connect(self.close)

        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'About'), 
            self, 
            icon=utils.doIcon('help-about'), 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        self.actionAbout.triggered.connect(self.about)

    def createMenusAndButtons(self):
        """
        blablabla
        """
        self.fileMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'File'), self)
        self.fileMenu.addAction(self.actionNew)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionSave)
        self.fileMenu.addAction(self.actionDoHtml)
        self.fileMenu.addAction(self.actionOpenNotesDir)
        self.fileMenu.addAction(self.actionOpenInTextEditor)
        if utils.OS_NAME[0] == 'linux':
            self.fileMenu.addAction(self.actionCreateDesktopFileLinux)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionQuit)

        self.helpMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Help'), self)
        self.helpMenu.addAction(self.actionAbout)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.helpMenu)

        # la toolbar :
        self.toolBar.addAction(self.actionQuit)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionNew)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addAction(self.actionDoHtml)
        self.toolBar.addAction(self.actionOpenNotesDir)
        self.toolBar.addAction(self.actionOpenInTextEditor)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionAbout)

    def find(self):
        query = self.findField.text()
        self.noteTextEdit.find(query)
        self.noteTextEdit.setFocus()

    def spellCheck(self):
        self.noteTextEdit.setSpellCheck(self.sender().isChecked())
        self.noteTextEdit.highlighter.rehighlight()
        self.noteTextEdit.setFocus()

    def noteChanged(self):
        """
        blablabla
        """
        if self.noNoteChanged:
            self.noNoteChanged = False
            return
        if not(self.testMustSave()):
            self.noNoteChanged = True
            noteWidget = self.notesListWidget.findItems(
                self.actual['note'], QtCore.Qt.MatchExactly)[0]
            self.notesListWidget.setCurrentItem(noteWidget)
            return
        # pour esquiver l'appel à textChanged :
        self.noChange = True
        noteWidget = self.notesListWidget.currentItem()
        note = noteWidget.text()
        fileName = noteWidget.data(QtCore.Qt.UserRole)
        noteContent = utils_filesdirs.readTextFile(fileName)
        self.noteTextEdit.setPlainText(noteContent)
        self.actual = {
            'note': note, 
            'fileName': fileName, 
            'mustSave': False}
        self.updateTitle()

    def textChanged(self):
        """
        pour prendre en compte dans le titre 
        le fait que le texte a été modifié.
        """
        if self.noChange:
            self.noChange = False
            self.noteTextEdit.setDisabled(False)
            return
        self.updateTitle(mustSave=True)

    def updateTitle(self, mustSave=False):
        """
        mise à jour du titre de la fenêtre.
        Suivant que le fichier est enregistré, a été modifié, ...
        """
        self.actual['mustSave'] = mustSave
        if mustSave:
            mustSaveText = '*'
        else:
            mustSaveText = ''
        if self.actual['note'] == '':
            title = '{0} {1}  []'.format(
                utils.PROGTITLE, utils.PROGVERSION)
        else:
            title = '{0} {1}  {2}[{3}]'.format(
                utils.PROGTITLE, 
                utils.PROGVERSION, 
                mustSaveText, 
                self.actual['note'])
        self.setWindowTitle(title)
        self.actionSave.setEnabled(mustSave)
        self.actionDoHtml.setEnabled(self.actual['note'] != '')

    def testMustSave(self):
        """
        pour savoir s'il faut proposer d'enregistrer le fichier.
        """
        result = True
        if self.actual['mustSave']:
            message = QtWidgets.QApplication.translate(
                'main',
                'The file has been modified.\n'
                'Do you want to save your changes?')
            message = '{0} :\n\n{1}'.format(self.actual['note'], message)
            reponseMustSave = utils_functions.messageBox(
                self, level='warning', message=message,
                buttons=['Save', 'Discard', 'Cancel'])
            if reponseMustSave == QtWidgets.QMessageBox.Cancel:
                result = False
            elif reponseMustSave == QtWidgets.QMessageBox.Save:
                self.saveFile()
        return result

    def showPreviewPanel(self):
        if self.previewPanelButton.isChecked():
            newWidth = 200 + (self.width() - 200) // 2
            self.hsplitter.moveSplitter(newWidth, 2)
        else:
            newWidth = QtWidgets.QApplication.desktop(
                ).availableGeometry().width()
            self.hsplitter.moveSplitter(newWidth, 2)
        self.noteTextEdit.setFocus()

    def splitterMoved(self, pos, index):
        if self.previewFrame.visibleRegion().isEmpty():
            self.previewPanelButton.setChecked(False)
        else:
            self.previewPanelButton.setChecked(True)

    def preview(self):
        self.saveFile()
        fileName = self.actual['fileName']
        outFileName = utils_filesdirs.md2html(self, fileName, replace=True)
        url = QtCore.QUrl().fromLocalFile(outFileName)
        self.previewWebView.load(url)

    def openPreview(self):
        fileName = self.actual['fileName']
        outFileName = utils_filesdirs.md2html(self, fileName)
        url = QtCore.QUrl().fromLocalFile(outFileName)
        QtGui.QDesktopServices.openUrl(url)

    def closeEvent(self, event):
        if not(self.testMustSave()):
            event.ignore()
            return
        self.closing = True

        # on vide le dossier temporaire :
        utils_filesdirs.emptyDir(self.tempPath)

        # on écrit le fichier de config :
        configDict = {'ORDER': []}
        for i in range(self.notesListWidget.count()):
            noteWidget = self.notesListWidget.item(i)
            note = noteWidget.text()
            configDict['ORDER'].append(note)
        configFileName = '{0}/config.json'.format(self.configDir.canonicalPath())
        configFile = open(
            configFileName, 'w', encoding='utf-8')
        json.dump(configDict, configFile, indent=4)
        configFile.close()
        event.accept()

    def newFile(self):
        """
        enregistrement d'un fichier.
        """
        if not(self.testMustSave()):
            return
        note, ok = QtWidgets.QInputDialog.getText(
            self, 
            QtWidgets.QApplication.translate('main', 'New'), 
            QtWidgets.QApplication.translate('main', 'New file name:'), 
            QtWidgets.QLineEdit.Normal, 
            '')
        if not(ok and note != ''):
            return
        noteFile = '{0}/{1}.md'.format(self.notesDir.path(), note)
        noteWidget = QtWidgets.QListWidgetItem(note)
        noteWidget.setData(QtCore.Qt.UserRole, noteFile)
        self.notesListWidget.addItem(noteWidget)
        self.notesListWidget.setCurrentItem(noteWidget)
        self.noteChanged()
        self.updateTitle(mustSave=True)

    def saveFile(self):
        """
        enregistrement d'un fichier.
        """
        fileName = self.actual['fileName']
        utils_functions.doWaitCursor()
        try:
            lines = self.noteTextEdit.toPlainText()
            QtCore.QFile(fileName).remove()
            outFile = QtCore.QFile(fileName)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << lines
                outFile.close()
            self.updateTitle()
        finally:
            utils_functions.restoreCursor()

    def doHtml(self):
        """
        retourne un fichier html d'après un fichier md (markdown)
        """
        # fichier final :
        outFileName = '{0}/html/{1}.html'.format(
            self.configDir.path(), 
            self.actual['note'])
        # s'il existe déjà, on le supprime :
        if QtCore.QFileInfo(outFileName).exists():
            QtCore.QFile(outFileName).remove()
        # fichier du modèle html à utiliser :
        templateFile = '{0}/html/default.html'.format(self.configDir.path())
        # récupération du contenu du modèle html :
        htmlLines = ''
        inFile = QtCore.QFile(templateFile)
        if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(inFile)
            stream.setCodec('UTF-8')
            htmlLines = stream.readAll()
            inFile.close()
        # récupération du fichier Markdown :
        mdLines = ''
        inFile = QtCore.QFile(self.actual['fileName'])
        if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(inFile)
            stream.setCodec('UTF-8')
            mdLines = stream.readAll()
            inFile.close()
        # on met en forme et on remplace le repère du modèle :
        mdLines = mdLines.replace('\n', '\\n').replace("'", "\\'")
        htmlLines = htmlLines.replace('# USER TEXT', mdLines)
        # on enregistre le nouveau fichier html :
        outFile = QtCore.QFile(outFileName)
        if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(outFile)
            stream.setCodec('UTF-8')
            stream << htmlLines
            outFile.close()
        # on ouvre le fichier :
        url = QtCore.QUrl().fromLocalFile(outFileName)
        QtGui.QDesktopServices.openUrl(url)

    def doCreateDesktopFileLinux(self):
        """
        Sous GNU/Linux, propose de créer un fichier .desktop
        pour lancer le logiciel.
        """
        title = QtWidgets.QApplication.translate(
            'main', 
            'Choose the Directory where the desktop file will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks 
            | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            result = utils_filesdirs.createDesktopFile(
                self, directory, 'myNotes', 'icon')

    def about(self):
        """
        affichage de la fenêtre À propos.
        """
        import utils_about
        aboutdialog = utils_about.AboutDlg(
            self, utils.LOCALE, icon='./images/logo.png')
        lastState = self.disableInterface(aboutdialog)
        aboutdialog.exec_()
        self.enableInterface(aboutdialog, lastState)

    def disableInterface(self, dialog):
        """
        cache les barres d'outils lors de l'appel d'un dialog
        affiché dans le stackedWidget.
        On retourne aussi :
            lastIndex : le widget affiché précédemment dans le stackedWidget
            lastTitle : le titre de la fenêtre
        """
        lastIndex = self.stackedWidget.currentIndex()
        lastTitle = self.windowTitle()
        for widget in (self.menuBar(), self.toolBar):
            widget.setVisible(False)
        self.setWindowTitle(dialog.windowTitle())
        newIndex = self.stackedWidget.addWidget(dialog)
        self.stackedWidget.setCurrentIndex(newIndex)
        return (lastIndex, lastTitle)

    def enableInterface(self, dialog, lastState=(0, '')):
        """
        replace les barres d'outils après l'appel d'un dialog
        affiché dans le stackedWidget.
        On remet aussi l'état précédent via lastState :
            widget affiché dans le stackedWidget
            titre de la fenêtre
        """
        if self.closing:
            return
        self.stackedWidget.removeWidget(dialog)
        self.stackedWidget.setCurrentIndex(lastState[0])
        self.setWindowTitle(lastState[1])
        for widget in (self.menuBar(), self.toolBar):
            widget.setVisible(True)

    def openNotesDir(self):
        utils_filesdirs.openDir(self.notesDir.path())

    def openInTextEditor(self):
        outFileName = '{0}/notes/{1}.md'.format(
            self.configDir.path(), 
            self.actual['note'])
        # on ouvre le fichier :
        url = QtCore.QUrl().fromLocalFile(outFileName)
        QtGui.QDesktopServices.openUrl(url)
