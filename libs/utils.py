# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of MyNotes project.
# Name:         MyNotes: a little notes soft
# Copyright:    (C) 2015-2021 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    blablabla
"""

# importation des modules utiles :
import sys
import os

from PyQt5 import QtCore, QtWidgets, QtGui

"""
****************************************************
    VERSIONS DE PYTHON, QT, ETC
****************************************************
"""

# version de Python :
PYTHONVERSION = sys.version_info[0] * 10 + sys.version_info[1]
print('PYTHONVERSION:', PYTHONVERSION)

# détection du système (nom et 32 ou 64) :
OS_NAME = ['', '']
def detectPlatform():
    global OS_NAME
    # 32 ou 64 bits :
    if sys.maxsize > 2**32:
        bits = 64
    else:
        bits = 32
    # platform et osName :
    platform = sys.platform
    osName = ''
    if platform.startswith('linux'):
        osName = 'linux'
    elif platform.startswith('win'):
        osName = 'win'
    elif platform.startswith('freebsd'):
        osName = 'freebsd'
    elif platform.startswith('darwin'):
        import platform
        if 'powerpc' in platform.uname():
            osName = 'powerpc'
        else:
            osName = 'mac'
    OS_NAME = [osName, bits]
detectPlatform()

MODEBAVARD = False
if 'MODEBAVARD' in sys.argv:
    MODEBAVARD = True



"""
****************************************************
    VARIABLES LIÉES AU LOGICIEL
****************************************************
"""

PROGTITLE = 'MyNotes'
PROGNAME = 'mynotes'
PROGVERSION = '1.0'
PROGAUTHOR = 'Pascal Peter'
PROGMAIL = 'pascal.peter at free.fr'
PROGWEB = 'http://pascal.peter.free.fr'
LICENCETITLE = 'GNU General Public License version 3'
HELPPAGE = 'http://pascal.peter.free.fr'









# la langue de l'interface :
LOCALE = ''

def changeLocale(newValue):
    global LOCALE
    LOCALE = newValue



SUPPORTED_IMAGE_FORMATS = ('png',)

def loadSupportedImageFormats():
    global SUPPORTED_IMAGE_FORMATS
    SUPPORTED_IMAGE_FORMATS = QtGui.QImageReader.supportedImageFormats()

def doIcon(fileName='', what='ICON'):
    ext = ''
    if len(fileName.split('.')) > 1:
        ext = fileName.split('.')[1]
        fileName = fileName.split('.')[0]
    if ext == '':
        for test in ('png', 'svgz'):
            if test in SUPPORTED_IMAGE_FORMATS:
                allFileName = 'images/{0}.{1}'.format(fileName, test)
                if QtCore.QFile(allFileName).exists():
                    ext = test
        allFileName = 'images/{0}.{1}'.format(fileName, ext)
        if ext == 'png':
            print('doIcon:', fileName)
    else:
        if ext in SUPPORTED_IMAGE_FORMATS:
            allFileName = 'images/{0}.{1}'.format(fileName, ext)
        else:
            allFileName = 'images/{0}.png'.format(fileName)
        if not(QtCore.QFile(allFileName).exists()):
            print('doIcon:', allFileName)
            allFileName = 'images/{0}.png'.format(fileName)
    if what == 'ICON':
        return QtGui.QIcon.fromTheme(fileName, QtGui.QIcon(allFileName))
    else:
        return QtGui.QPixmap(allFileName)






