# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of MyNotes project.
# Name:         MyNotes: a little notes soft
# Copyright:    (C) 2015-2021 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Une fenêtre à 2 listes avec drag drop
"""

# importation des modules utiles :

from PyQt5 import QtCore, QtWidgets, QtGui



"""
ITEMS_IN_DRAGDROP est une liste globale permettant 
de savoir quels items sont déplacés,
et depuis quelle liste.
ITEMS_IN_DRAGDROP = [num, [item1, item2, ...]]
    num est le numéro de la liste d'où 
    proviennent les items (et -1 si pas d'item)
    [item1, item2, ...] est la liste des items
"""
ITEMS_IN_DRAGDROP = []


class DragDropListWidget(QtWidgets.QListWidget):
    """
    la classe utilisée pour les 2 listes.
    Si extendedSelection est à True, 
    on peut sélectionner plusieurs items.
    Si acceptMoves est à True, 
    un drag-drop interne permettra de déplacer les items.
    """
    def __init__(self, parent=None, extendedSelection=True, acceptMoves=True):
        """
        initialisation
        """
        super(DragDropListWidget, self).__init__(parent)
        self.parent = parent

        if extendedSelection:
            self.setSelectionMode(
                QtWidgets.QAbstractItemView.ExtendedSelection)
        self.acceptMoves = acceptMoves
        self.setDragEnabled(True)
        self.setMovement(QtWidgets.QListView.Snap)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.drop = False

    def startDrag(self, supportedActions):
        """
        Début d'un drag.
        On met self.num et la liste des items 
        dans la liste globale ITEMS_IN_DRAGDROP.
        """
        global ITEMS_IN_DRAGDROP
        ITEMS_IN_DRAGDROP = []
        for item in self.selectedItems():
            ITEMS_IN_DRAGDROP.append(item)
        return super(DragDropListWidget, self).startDrag(supportedActions)

    def dragEnterEvent(self, event):
        """
        On gère les cas où le drop sera autorisé.
        """
        if self.acceptMoves:
            super(DragDropListWidget, self).dragEnterEvent(event)
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        Fin d'un drag-drop.
        Si self.acceptMoves est à False, on doit recréer les items
        avant de les ajouter.
        """
        if not(self.acceptMoves):
            for item in ITEMS_IN_DRAGDROP:
                # on doit recréer un nouvel item, sinon ça ne fonctionne pas :
                newItem = QtWidgets.QListWidgetItem(item.text())
                newItem.setData(
                    QtCore.Qt.UserRole, item.data(QtCore.Qt.UserRole))
                newItem.setToolTip(item.toolTip())
                newItem.setIcon(item.icon())
                # et on l'ajoute à la fin de la liste :
                self.addItem(newItem)
        else:
            super(DragDropListWidget, self).dropEvent(event)
        # on passe ensuite au traitement d'après drop :
        self.drop = True
        self.doAfterDrop()

    def doAfterDrop(self):
        """
        Après un drag-drop.
        Si self.parent a bien une fonction doAfterDrop 
        pour traiter l'après drag-drop
        (gérer les 2 listes), on l'appelle.
        Sinon, on appelle self.removeAfterDrop.
        """
        try:
            self.parent.doAfterDrop()
        except:
            self.removeAfterDrop()

    def removeAfterDrop(self):
        """
        Après un drop, si les items viennent de la liste,
        on supprime les items.
        Enfin on vide la liste globale ITEMS_IN_DRAGDROP.
        """
        global ITEMS_IN_DRAGDROP
        for item in ITEMS_IN_DRAGDROP:
            self.takeItem(self.row(item))
        ITEMS_IN_DRAGDROP = []

