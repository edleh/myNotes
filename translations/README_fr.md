# MyNotes

* **Site Web :** http://pascal.peter.free.fr
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2015-2021

----

#### DÉPENDANCES
* [Python](https://www.python.org) : langage de programmation
* [Qt](https://www.qt.io) : "toolkit" très complet (interface graphique et tout un tas de choses)
* [PyQt](https://riverbankcomputing.com) : lien entre Python et Qt

#### Autres bibliothèques utilisées et trucs divers
* [pyspellchecker](https://github.com/barrust/pyspellchecker) : pour le correcteur d'orthographe
* [marked](https://github.com/markedjs/marked) : pour afficher les fichiers Markdown
* [Bootstrap](https://getbootstrap.com) : un framework CSS/JS
* [Breeze Icons](https://api.kde.org/frameworks/breeze-icons/html/index.html)
* [Blender](https://www.blender.org) : pour la création du logo

