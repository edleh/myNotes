SOURCES      += ../myNotes.pyw

SOURCES      += ../libs/main.py
SOURCES      += ../libs/utils_about.py
SOURCES      += ../libs/utils_functions.py

TRANSLATIONS += ../translations/mynotes.ts
TRANSLATIONS += ../translations/mynotes_fr.ts
