<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/utils_about.py" line="84"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="87"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="92"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="63"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="89"/>
        <source>information message</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="90"/>
        <source>question message</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="91"/>
        <source>warning message</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="92"/>
        <source>critical message</source>
        <translation>Message critique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="274"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="406"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation>Le fichier a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="132"/>
        <source>END !</source>
        <translation>TERMINÉ !</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="133"/>
        <source>Images are saved in the folder:</source>
        <translation>Les images sont enregistrées dans le dossier :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="262"/>
        <source>Open Notes Dir</source>
        <translation>Ouvrir le dossier des notes</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="66"/>
        <source>(version {0})</source>
        <translation>(version {0})</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="156"/>
        <source>Search</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="256"/>
        <source>Transform to html</source>
        <translation>Transformer en html</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="195"/>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="167"/>
        <source>Spell check</source>
        <translation>Vérificateur d&apos;orthographe</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="201"/>
        <source>Open in browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="477"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="249"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="281"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="299"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="312"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="477"/>
        <source>New file name:</source>
        <translation>Nom du nouveau fichier :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="176"/>
        <source>Show/Hide Preview Panel</source>
        <translation>Afficher/Masquer le panneau de prévisualisation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="268"/>
        <source>Open in text editor</source>
        <translation>Ouvrir dans l&apos;éditeur de texte</translation>
    </message>
</context>
</TS>
